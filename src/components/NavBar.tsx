import React from 'react';
import { Link } from 'react-router-dom';
//import 'bootstrap/scss/bootstrap.scss';

function NavBar() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <Link className="navbar-brand" to="/">
        Chollos Hardware
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="nav-link" to="/publicaciones">
              Publicaciones
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/nueva-publicacion">
              Nueva Publicación
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/membresia">
              Membresía
            </Link>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="https://diarioinformatico.com">
              Blog
            </a>
          </li>
        </ul>
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/login">
              Login
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
