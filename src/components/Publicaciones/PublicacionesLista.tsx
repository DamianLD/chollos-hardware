import React, { useEffect, useState } from 'react';
import * as publicacionService from './PublicacionService';
import { Publicacion } from './Publicacion';
import { PublicacionItem } from './PublicacionItem';

export const PublicacionesLista = () => {
  const [publicaciones, setPublicaciones] = useState<Publicacion[]>([]);

  const cargarPublicaciones = async () => {
    const publicacionesBD = await publicacionService.getPublicaciones();
    const publicacionesRecientes = publicacionesBD.data
      .map((publicacion) => {
        return {
          ...publicacion,
          createdAt: publicacion.createdAt
            ? new Date(publicacion.createdAt)
            : new Date(),
          updatedAt: publicacion.updatedAt
            ? new Date(publicacion.updatedAt)
            : new Date(),
        };
      })
      .sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
    setPublicaciones(publicacionesBD.data);
  };

  useEffect(() => {
    cargarPublicaciones();
  }, []);

  return (
    <div className="row">
      {publicaciones.map((publicacion) => {
        return (
          <PublicacionItem
            key={publicacion._id}
            publicacion={publicacion}
            cargarPublicaciones={cargarPublicaciones}
          />
        );
      })}
    </div>
  );
};
