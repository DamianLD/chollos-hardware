import React from 'react';
import './ContenedorLogin.scss';
import { LoginFormulario } from './LoginFormulario';

function ContenedorLogin() {
  return (
    <div className="app-container">
      <div className="formulario">
        <h1>Chollos Hardware</h1>
        <img
          className="login-image"
          src="https://render.fineartamerica.com/images/rendered/square-dynamic/small/images/artworkimages/mediumlarge/1/gandalf-tom-carlton.jpg"
          //src="https://avatarfiles.alphacoders.com/209/thumb-209735.jpg"
        />
        <LoginFormulario />
      </div>
    </div>
  );
}

export default ContenedorLogin;
