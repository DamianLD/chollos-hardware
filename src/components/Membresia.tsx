import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
// @ts-ignore
import StarIcon from '@material-ui/icons/StarBorder';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

const tiers = [
  {
    title: 'Gratuito',
    price: '0',
    description: ['2 alertas', '2 publicaciones'],
  },
  {
    title: 'Profesional',
    subheader: 'El más popular',
    price: '3',
    description: ['10 alertas', '10 publicaciones'],
  },
  {
    title: 'Empresarial',
    price: '9',
    description: ['Infinitas alertas', 'Infinitas publicaciones'],
  },
];

export default function Membresia() {
  return (
    <React.Fragment>
      {/* Hero unit */}
      <Container maxWidth="sm" component="main">
        <Typography component="h1" variant="h2" align="center" gutterBottom>
          Planes de precios
        </Typography>
        <Typography variant="h5" align="center" component="p" paragraph>
          Aquí podrás mejorar la membresía para obtener un mayor número de
          alertas y prublicaciones
        </Typography>
      </Container>
      {/* End hero unit */}
      <Container maxWidth="md" component="main">
        <Grid container spacing={5} alignItems="flex-end">
          {tiers.map((tier) => (
            <Grid
              item
              key={tier.title}
              xs={12}
              sm={tier.title === 'Empresarial' ? 12 : 6}
              md={4}
            >
              <Card>
                <CardHeader
                  title={tier.title}
                  subheader={tier.subheader}
                  titleTypographyProps={{ align: 'center' }}
                  subheaderTypographyProps={{ align: 'center' }}
                  action={tier.title === 'Profesional' ? <StarIcon /> : null}
                />
                <CardContent>
                  <div>
                    <Typography
                      component="h2"
                      variant="h3"
                      color="textPrimary"
                      align="center"
                    >
                      {tier.price} €
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h6"
                      color="textSecondary"
                      align="right"
                    >
                      /mes
                    </Typography>
                  </div>
                  <ul>
                    {tier.description.map((line) => (
                      <Typography
                        component="li"
                        variant="subtitle1"
                        align="center"
                        key={line}
                      >
                        {line}
                      </Typography>
                    ))}
                  </ul>
                </CardContent>
                <CardActions></CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </React.Fragment>
  );
}
