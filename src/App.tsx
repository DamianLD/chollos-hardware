import * as React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { PublicacionesLista } from './components/Publicaciones/PublicacionesLista';
import Filtros from './components/Filtros/Filtros';
import NavBar from './components/NavBar';
import WaveBorder from './components/WaveBorder';
import { PublicacionFormulario } from './components/Publicaciones/PublicacionFormulario';
import Membresia from './components/Membresia';
import { ToastContainer } from 'react-toastify';
import { LoginFormulario } from './components/LoginFormulario';
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';
import ContenedorLogin from './components/ContenedorLogin';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route path="/login">
          <ContenedorLogin />
        </Route>
        <Route path="/publicaciones">
          <div className="container p-4">
            <PublicacionesLista />
          </div>
        </Route>
        <Route path="/membresia">
          <div className="container p-4">
            <Membresia />
          </div>
        </Route>
        <Route path="/nueva-publicacion">
          <div className="container p-4">
            <PublicacionFormulario />
          </div>
        </Route>
        <Route path="/actualizar/:id">
          <div className="container p-4">
            <PublicacionFormulario />
          </div>
        </Route>
        <Route path="/">
          <div className="container p-4">
            <h1>Bienvenidos a chollos Hardware</h1>
          </div>
        </Route>
      </Switch>
      <ToastContainer />
    </BrowserRouter>
  );
}

export default App;
