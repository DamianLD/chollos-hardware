import React from 'react';
import { useHistory } from 'react-router-dom';
import { Publicacion } from './Publicacion';
import { deletePublicacion } from './PublicacionService';
import './PublicacionItem.scss';

interface Props {
  publicacion: Publicacion;
  cargarPublicaciones: () => void;
}

export const PublicacionItem = ({
  publicacion,
  cargarPublicaciones,
}: Props) => {
  const history = useHistory();

  const obtenerFecha = (publicacion: any) => {
    const fecha = new Date(publicacion.createdAt);
    const fechaFormateada = `Publicado el ${fecha.getDate()}/${
      fecha.getMonth() + 1
    }/${fecha.getFullYear()}`;
    return fechaFormateada;
  };

  const handleEliminar = async (id: string) => {
    await deletePublicacion(id);
    cargarPublicaciones();
  };

  return (
    <div className="col-md-4 ">
      <div className="card card-body publicacion-card">
        <div className="d-flex justify-content-between">
          <h1
            onClick={() => history.push(`/actualizar/${publicacion._id}`)}
            style={{ cursor: 'pointer' }}
          >
            {publicacion.titulo}
          </h1>
          <span
            className="text-danger"
            onClick={() => publicacion._id && handleEliminar(publicacion._id)}
            style={{ cursor: 'pointer' }}
          >
            X
          </span>
        </div>
        <p>{publicacion.descripcion}</p>
        <p>Precio: {publicacion.precio} €</p>
        <p>{obtenerFecha(publicacion)}</p>
      </div>
    </div>
  );
};
