import axios from 'axios';
import { Publicacion } from './Publicacion';

const API = 'https://cholloshardware.com:4000';

export const getPublicaciones = async () => {
  return await axios.get<Publicacion[]>(`${API}/publicaciones`);
};

export const getPublicacion = async (id: string) => {
  return await axios.get<Publicacion>(`${API}/publicaciones/${id}`);
};

export const createPublicacion = async (publicacion: Publicacion) => {
  return await axios.post(`${API}/publicaciones`, publicacion);
};

export const updatePublicacion = async (
  id: string,
  publicacion: Publicacion
) => {
  return await axios.put<Publicacion>(
    `${API}/publicaciones/${id}`,
    publicacion
  );
};

export const deletePublicacion = async (id: string) => {
  return await axios.delete<Publicacion>(`${API}/publicaciones/${id}`);
};
