module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'prettier',
    'plugin:react/recommended',
    'prettier/@typescript-eslint',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['react', '@typescript-eslint', 'prettier'],
  rules: {
    quotes: [2, 'single', { avoidEscape: true }],
    'react/jsx-filename-extension': [1, { extensions: ['.tsx'] }],
    'prettier/prettier': ['error'],
    'import/extensions': [0, 'ignorePackages'],
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': [1],
  },
};
