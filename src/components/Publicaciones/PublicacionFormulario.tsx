import React, { ChangeEvent, FormEvent, useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Publicacion } from './Publicacion';
import * as publicacionService from './PublicacionService';

type InputChange = ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;
interface Params {
  id: string;
}

export const PublicacionFormulario = () => {
  const history = useHistory();
  const params = useParams<Params>();

  const estadoInicial = {
    titulo: '',
    descripcion: '',
  };

  const [precioInicial, setPrecioInicial] = useState<string>('');
  const [precioInicialInt, setPrecioInicialInt] = useState<Number>(0);
  const [publicacion, setPublicacion] = useState<Publicacion>(estadoInicial);

  const handleInputChange = (e: InputChange) => {
    setPublicacion({ ...publicacion, [e.target.name]: e.target.value });
  };

  const handlePrecioChange = (e: InputChange) => {
    setPrecioInicial(e.target.value);
  };

  const esActualizacion = () => {
    return params.id;
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!esActualizacion()) {
      if (precioInicial && publicacion.titulo && publicacion.descripcion) {
        if (precioInicial !== '0') {
          publicacion.precio = Number(precioInicial);
          await publicacionService.createPublicacion(publicacion);
          toast.success('Publicación añadida exitosamente!');
          history.push('/publicaciones');
        } else {
          toast.error('El precio no puede ser 0');
        }
      } else {
        toast.error('No se han añadidos todos los campos');
      }
    } else {
      publicacion.precio = Number(precioInicial);
      await publicacionService.updatePublicacion(params.id, publicacion);
      history.push('/publicaciones');
    }
  };

  const getPublicacion = async (id: string) => {
    const requestPublicacion = await publicacionService.getPublicacion(id);
    const publicacion = requestPublicacion.data;
    setPublicacion(publicacion);
    if (publicacion.precio) {
      setPrecioInicial(publicacion.precio.toString());
    }
  };

  useEffect(() => {
    if (params.id) {
      getPublicacion(params.id);
    }
  }, []);

  return (
    <div className="row">
      <div className="col-md-4 offset-md-4">
        <div className="card">
          <div className="card-body">
            {params.id ? (
              <h3>Editar publicación</h3>
            ) : (
              <h3>Nueva publicación</h3>
            )}
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <input
                  type="text"
                  name="titulo"
                  placeholder="¿Qué estás vendiendo?"
                  className="form-control"
                  onChange={handleInputChange}
                  autoFocus
                  value={publicacion.titulo}
                />
              </div>
              <div className="form-group">
                <textarea
                  className="form-control"
                  name="descripcion"
                  rows={3}
                  placeholder="Escribe una descripcion"
                  onChange={handleInputChange}
                  value={publicacion.descripcion}
                ></textarea>
              </div>
              <div className="form-group">
                <input
                  type="number"
                  name="precio"
                  placeholder="Precio del producto"
                  onChange={handlePrecioChange}
                  value={precioInicial}
                />
              </div>
              {params.id ? (
                <button type="submit" className="btn btn-info">
                  Actualizar publicación
                </button>
              ) : (
                <button type="submit" className="btn btn-primary">
                  Crear publicación
                </button>
              )}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
