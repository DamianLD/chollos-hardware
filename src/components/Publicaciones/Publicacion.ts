export interface Publicacion {
  titulo: string;
  descripcion: string;
  precio?: number;
  createdAt?: string;
  updatedAt?: string;
  _id?: string;
}
